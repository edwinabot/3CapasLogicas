<?php
/**
 * Created by PhpStorm.
 * User: edwin
 * Date: 24/10/16
 * Time: 11:30
 */

namespace vistas;


/**
 * Interface DPGInterface
 * Dispatch, Post, Get.
 * @package controladoras
 */
interface DPGInterface
{

    /**
     * Resuelve pedidos que vienen por GET
     * @param $request
     * @return mixed
     */
    function get($GET);

    /**
     * Resuelve pedidos que vienen por POST
     * Debe manejar
     * @param $request
     * @return mixed
     */
    function post($POST);

    /**
     * Despacha el pedido a get() o post() dependiendo del método HTTP del request
     * @param $despachar
     * @return mixed
     */
    function dispatch();
}