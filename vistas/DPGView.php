<?php
/**
 * Created by PhpStorm.
 * User: edwin
 * Date: 24/10/16
 * Time: 11:37
 */

namespace vistas;
require_once("DPGInterface.php");

abstract class DPGView implements DPGInterface
{
    function dispatch()
    {

        if($_SERVER['REQUEST_METHOD'] ==  'POST'){
           $this->post($_POST);
        } else {
            $this->get($_GET);
        }

    }
}