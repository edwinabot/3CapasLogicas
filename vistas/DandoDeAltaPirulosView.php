<?php
/**
 * Created by PhpStorm.
 * User: edwin
 * Date: 24/10/16
 * Time: 11:13
 */
namespace vistas;
require_once("DPGView.php");
require_once("../controladoras/DandoDeAltaPirulosController.php");
require_once("../modelos/Pirulo.php");

use controladoras\DandoDeAltaPirulosController;
use Exception;
use modelos\Pirulo;

/*
 * Creamos la clase de la vista para hacer los overrides de get y post
 * La vamos a hacer singleton ya que necesitamos solo una instancia
 * y a diferencia de una clase con atributos estáticos podemos tener lógica
 * de inicialización en el constructor como corresponde
 */

class DandoDeAltaPirulosView extends DPGView
{
    private static $instancia;

    public $formulario;

    /**
     * DandoDeAltaPirulosView constructor.
     * @param $formulario
     */
    private function __construct()
    {
        $this->formulario = ["numeros"=>null, "letras"=>null];
    }

    public static function getInstancia()
    {
        if ( is_null( self::$instancia ) )
        {
            self::$instancia = new self();
        }
        return self::$instancia;
    }

    private function crearPiruloConElFormulario(){
        try{
            return new Pirulo($this->formulario['numeros'], $this->formulario['letras']);
        } catch (Exception $e){
            var_dump($e);
        }
    }

    private function setearFormularioConPost($valores_posteados){
        if(!empty($valores_posteados)){
            $this->formulario = $valores_posteados;
        } else {
            throw new Exception("El formulario vino vacío");
        }
    }

    /**
     * Resuelve pedidos que vienen por GET
     * @param $request
     * @return mixed
     */
    function get($GET)
    {
        // TODO: Implement get() method. Acá hago algo con los valores que vienen por get
        var_dump($GET);
    }

    /**
     * Resuelve pedidos que vienen por POST
     * Debe manejar
     * @param $request
     * @return mixed
     */
    function post($POST)
    {
        // TODO: Implement post() method. Acá hago algo con los valores que vienen por post
        try{
            $this->setearFormularioConPost($POST);
            $p = $this->crearPiruloConElFormulario();
            $controladora = new DandoDeAltaPirulosController();
            $controladora->DarDeAltaUnPirulo($p);
        } catch (Exception $e){

        }
    }
}

/*
 * Hcaemos el dispatch del request
 */
$objeto_vista = DandoDeAltaPirulosView::getInstancia();
$objeto_vista->dispatch();

?>


<html>
<head>
    <title>Dando de alta pirulos</title>
</head>
<body>
<h2>Dando de alta pirulos</h2>

<form action="./DandoDeAltaPirulosView.php" method="post">

    <label for="numeros">Numeros</label>
    <input type="number" name="numeros" value="<?php echo $objeto_vista->formulario['numeros'] ?>">
    <label for="letras">Letras</label>
    <input type="text" name="letras" value="<?php echo $objeto_vista->formulario['letras'] ?>">

    <input type="submit" value="Enviar">
</form>
<a href="../index.php">Inicio</a>

</body>
</html>
