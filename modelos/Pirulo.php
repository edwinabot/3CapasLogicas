<?php
/**
 * Created by PhpStorm.
 * User: edwin
 * Date: 24/10/16
 * Time: 14:43
 */

namespace modelos;


use UnexpectedValueException;

class Pirulo
{
    private $letras;
    private $numeros;

    /**
     * Pirulo constructor.
     * @param $letras
     * @param $numeros
     */
    public function __construct($numeros, $letras)
    {
        $this->setLetras($letras);
        $this->setNumeros($numeros);
    }

    /**
     * @return mixed
     */
    public function getLetras()
    {
        return $this->letras;
    }

    /**
     * @param mixed $letras
     */
    public function setLetras($letras)
    {
        $this->letras = $letras;
    }

    /**
     * @return mixed
     */
    public function getNumeros()
    {
        return $this->numeros;
    }

    /**
     * @param mixed $numeros
     */
    public function setNumeros($numeros)
    {
        if(is_numeric($numeros)){
            $this->numeros = $numeros;
        } else {
            $e = new UnexpectedValueException("Los numeros tienen que ser todos numeros");
            throw $e;
        }
    }


}